package fr.cnam.foad.nfa035.badges.wallet.dao.impl.json;

import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.IOException;
import java.io.OutputStream;
import java.util.SortedMap;

/**
 * Interface spécifiant le comportement élémentaire d'un DAO destiné à la gestion de badge digitaux par accès direct
 * et donc nécessite la prise en compte de métadonnées encodée au format JSON
 */
public interface JSONBadgeWalletDAO extends DirectAccessBadgeWalletDAO {


    /**
     * permet d'extraire les métadonnées des badges sur le flux d'écriture
     * @param imageStream
     * @param meta
     * @throws IOException
     */
    void getBadgeFromMetadata(OutputStream imageStream, DigitalBadgeMetadata meta) throws IOException;


    /**
     *méthode permettant d'afficher les métadonnées sous forme d'objet MAP triée
     * @return SortedMap<DigitalBadge, DigitalBadgeMetadata>
     * @throws IOException
     */
    SortedMap<DigitalBadge, DigitalBadgeMetadata> getWalletMetadataMap() throws IOException;
}
