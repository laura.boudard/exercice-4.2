package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.DirectAccessDatabaseDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.IOException;

/**
 * Interface permettant de rationnaliser les méthodes de désérialisation dans le but d'alléger le code
 */
public interface JSONWalletBonusDeserializer extends DirectAccessDatabaseDeserializer {

    /**
     * retourne un badge désérialisé
     * @param media
     * @param metas
     * @return
     * @throws IOException
     */
     DigitalBadge deserialize(WalletFrameMedia media, DigitalBadgeMetadata metas) throws IOException;

    /**
     * retourne un badge désérialisé à partir de sa position dans le wallet
     * @param media
     * @param pos
     * @return
     * @throws IOException
     */
     DigitalBadge deserialize(WalletFrameMedia media, long pos) throws IOException;
 }
