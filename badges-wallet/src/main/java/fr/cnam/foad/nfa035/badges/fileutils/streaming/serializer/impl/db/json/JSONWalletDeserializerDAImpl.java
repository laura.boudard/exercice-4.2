package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.DirectAccessDatabaseDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.commons.codec.binary.Base64InputStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.Set;

/**
 * Implémentation Base64 de désérialiseur de Badge Digital, basée sur des flux sur base JSON.
 */
public class JSONWalletDeserializerDAImpl implements DirectAccessDatabaseDeserializer,JSONWalletBonusDeserializer {

    private static final Logger LOG = LogManager.getLogger(JSONWalletDeserializerDAImpl.class);

    private OutputStream sourceOutputStream;
    Set<DigitalBadge> metas;

    /**
     * Constructeur élémentaire
     * @param sourceOutputStream
     * @param metas les métadonnées du wallet, si besoin
     */
    public JSONWalletDeserializerDAImpl(OutputStream sourceOutputStream, Set<DigitalBadge> metas) {
        this.setSourceOutputStream(sourceOutputStream);
        this.metas = metas;
    }

    /**
     * {@inheritDoc}
     *
     * @param media
     * @throws IOException
     */
    @Override
    public void deserialize(WalletFrameMedia media, DigitalBadge targetBadge) throws IOException {

        long pos = targetBadge.getMetadata().getWalletPosition();
        media.getChannel().seek(pos);
        deserialize(media,pos);
    }

    /**
     * {@inheritDoc}
     * @return OutputStream
     */
    @Override
    public OutputStream getSourceOutputStream() {
        return sourceOutputStream;
    }

    /**
     * {@inheritDoc}
     * @param os
     */
    @Override
    public void setSourceOutputStream(OutputStream os) {
        this.sourceOutputStream = os;
    }


    /**
     * {@inheritDoc}
     *
     * @param data
     * @return
     * @throws IOException
     */
    @Override
    public InputStream getDeserializingStream(String data) throws IOException {
        return new Base64InputStream(new ByteArrayInputStream(data.getBytes()));
    }

    /**
     * @param media
     * @param metas
     * @return
     * @throws IOException
     */
    @Override
    public DigitalBadge deserialize(WalletFrameMedia media, DigitalBadgeMetadata metas) throws IOException {
        long pos = metas.getWalletPosition();
        media.getChannel().seek(pos);

        return deserialize(media, pos);
    }

    /**
     * retourne le badge désérialisé
     * @param media
     * @param pos
     * @return
     * @throws IOException
     */
    @Override
    public DigitalBadge deserialize(WalletFrameMedia media, long pos) throws IOException {

        pos = metas.iterator().next().getMetadata().getWalletPosition();
        media.getChannel().seek(pos);

        JsonFactory jsonFactory = new JsonFactory();
        jsonFactory.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET,false);
        ObjectMapper objectMapper = new ObjectMapper(jsonFactory);

        String data = media.getEncodedImageReader(false).readLine();
        String badgeData = data.split(",\\{\"payload")[0].split(".*badge\":")[1];
        DigitalBadge badge = objectMapper.readValue(badgeData,DigitalBadge.class);

        String encodedImageData = data.split(",\\{\"payload\":")[1].split("\"\\}]")[0];
        // Désérialisation de l'image Base64 + écriture en clair dans le flux de restitution au format source
        try (OutputStream os = getSourceOutputStream()) {
            getDeserializingStream(encodedImageData).transferTo(os);
        }

        badge.setSerial(metas.iterator().next().getSerial());
        badge.setBegin(metas.iterator().next().getBegin());
        badge.setEnd(metas.iterator().next().getEnd());
        DigitalBadge originBadge = objectMapper.readValue(badgeData,DigitalBadge.class);
        refreshBadgeMetadata(originBadge, badge);

        return badge;
    }

    /**
     * {@inheritDoc}
     *
     * Inutile => Non pris encharge par cet Objet
     *
     * @param media
     * @throws IOException
     */
    @Override
    @Deprecated
    public void deserialize(WalletFrameMedia media) throws IOException {
        // Inutile
        throw new IOException("Non pris encharge par cet Objet");
    }

    private void refreshBadgeMetadata(DigitalBadge originBadge, DigitalBadge targetBadge) {
        if(originBadge.getSerial() != targetBadge.getSerial())
        {
            LOG.info("erreur de données du badge, le numéro de série ne correspond pas");
        }
    }

}
